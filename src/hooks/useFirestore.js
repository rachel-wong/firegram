import React from 'react'
import { useState, useEffect } from 'react'
import { projectFirestore} from '../firebase/config'

// reusable code for getting data from firestore
// collection arg is a string which is name
const useFirestore = (collectionName) => {
  const [docs, setDocs] = useState([]) // list of docs from the db collection

  // connect with db
  useEffect(() => {
    // listening for changes to the collection and takes a snapshot of all the docs in the collection
    const unsub = projectFirestore
      .collection(collectionName)
      .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {
        // snapshot = a snapshot of the entire collection whenever the collection is changed
        let documents = []
        snapshot.forEach(entry => {
          // push an object with all the properties inside each entry in the db collections (createdAt, url) and the id of the document
          documents.push({...entry.data(), id: entry.id})
        })

        setDocs(documents)
      })

    // cleanup function - unsubscribe when no longer needed ie. when the image grid is unmount (not showing this component)
    return () => unsub()
  }, [collectionName]) // triggers whenever the collectionName changes

  return {docs}
}

export default useFirestore