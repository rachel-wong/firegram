import { useState, useEffect } from 'react';
import { projectFirestore, projectStorage, timestamp } from '../firebase/config'

// Reusable logic for handling upload and returning info
const useStorage = (file) => {
  const [progress, setProgress] = useState(0)
  const [error, setError] = useState(null)
  const [url, setUrl] = useState(null)

  // new file value will trigger upload
  useEffect(() => {

    // references
    const storageRef = projectStorage.ref(file.name) // creates a firebase reference

    const collectionRef = projectFirestore.collection('images') // create a collection in the db called images

    // assign the file itself to the firebase reference
    storageRef.put(file).on('state_changed', (snap) => {
      let percentageProgress = (snap.bytesTransferred / snap.totalBytes) * 100 // set percentage of upload
      setProgress(percentageProgress)
    }, (error) => {
        setError(error)
    }, async () => {
        // when fully complete
        const url = await storageRef.getDownloadURL() // saved in firestore db
        const createdAt = timestamp()

        await collectionRef.add({url, createdAt }) // commit the file url to the database
        setUrl(url)
    })
  }, [file]) // fires this function whenever a file is present in the input field

  return {progress, error, url}
}

export default useStorage
