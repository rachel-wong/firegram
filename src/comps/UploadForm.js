import React, { useState }  from 'react';
import ProgressBar from './ProgressBar'

const UploadForm = () => {
  const [file, setFile] = useState(null)
  const [error, setError] = useState(null)

  const allowedTypes = ['image/png', 'image/jpeg']

  const changeHandler = (e) => {
    e.preventDefault()
    let selected = e.target.files[0]

    if (selected && allowedTypes.includes(selected.type)) {
      setError(null)
      setFile(selected)
    } else {
      setFile(null)
      setError("Please select an image file (PNG or JPEG)")
    }
    console.log("selected", selected)
  }
  return (
    <form>
      <label htmlFor="upload">
        <input id="upload" type="file" className="uploadButton" onChange={changeHandler}></input>
        +</label>
      <div className="output">
        {error && <div className="error">{error}</div>}
        {file && <div className="file">{file.name}</div>}
        {file && <ProgressBar file={file} setFile={setFile} />}
        {/* pass the setFile function to ProgressBar component so that on complete progress, setFile can be used to set file back to null and allow for another upload */}
      </div>
    </form>
  )
}

export default UploadForm