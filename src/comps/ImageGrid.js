import React from 'react'
import useFirestore from '../hooks/useFirestore'
import uploadImage from '../undraw_image_upload_wqh3.svg'
import { motion } from 'framer-motion'

const ImageGrid = ({ setSelectedImg }) => {

  const { docs } = useFirestore('images'); // listens to db and return entries

  // fallback image if broken link
  function defaultSrc(ev){
    ev.target.src = "../undraw_page_not_found_su7k.svg";
  }

  return (
    <>
      <div className="img-grid">
        {docs && docs.map((item, idx) => (
          <motion.div
            className="img-wrap"
            key={idx}
            layout
            onClick={() => { setSelectedImg(item.url) }}
            whileHover={{ opacity: 1 }}>
            <motion.img src={item.url} alt="test" onError={() => { defaultSrc() }} initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={{ delay: 1}} />
          </motion.div>
        ))}
      </div>

      {docs.length == 0 &&
        <motion.div className="no-images" initial={{ opacity: 0}} animate={{ opacity: 1 }} >
          <h3>No images yet, upload some. </h3>
          <motion.img src={ uploadImage } alt="Upload some images" width="200" height="200"/>
        </motion.div>
      }
    </>
  )
}

export default ImageGrid