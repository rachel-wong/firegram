import React, { useEffect, useState } from 'react'
import useStorage from '../hooks/useStorage'
import { motion } from 'framer-motion'

const ProgressBar = ({ file, setFile }) => {
  const { url, progress, error } = useStorage(file) // immediately starts up the useStorage hook, upload to firebase, get reference and return the progress if any

  useEffect(() => {
    if (url) {
      setFile(null)
    }
   }, [url, setFile])

  return (
  <>
      <motion.div className="progress-bar" initial={{width: 0}} animate={{ width: progress + '%' }}></motion.div>
  </>
  )
}

export default ProgressBar
