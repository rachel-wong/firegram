import React from 'react';

const Title = () => {
  return (
    <div className="title">
      <h1>FireGram</h1>
      <h2>Picture Gallery</h2>
      <p>Image upload, gallery functionalities, Firebase in react</p>
    </div>
  )
}

export default Title;