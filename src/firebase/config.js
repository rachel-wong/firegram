import firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/firestore'

  var firebaseConfig = {
    apiKey: "AIzaSyDQ_wnNgV0guc-jl6eEudGVqeBtPrtm8Tk",
    authDomain: "firegram-8322a.firebaseapp.com",
    databaseURL: "https://firegram-8322a-default-rtdb.firebaseio.com",
    projectId: "firegram-8322a",
    storageBucket: "firegram-8322a.appspot.com",
    messagingSenderId: "216720231380",
    appId: "1:216720231380:web:6230a3f708c698972aa441"
  };

firebase.initializeApp(firebaseConfig);

// initialise storage and firestore services
const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore(); // to interact with db

const timestamp = firebase.firestore.FieldValue.serverTimestamp; // get created at timestamp to add to file commit

export { projectStorage, projectFirestore, timestamp} // to make the firebase connection instances available to other parts of the project to commit/retrieve data form the backend.